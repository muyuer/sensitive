package  .aop.desensitization.annotation;

import java.lang.annotation.*;


/**
 * 在方法上声明 将方法返回对象中或者参数的敏感字段 加密/格式化
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface SensitiveEncode {

    /**
     * 指明需要脱敏的实体类class
     * 请注意，如果内嵌对象，只要指明内嵌对象的对象.class 也就是泛型T
     * @return
     */
    Class entity();


    /**
     * 指明需要加密的是参数还是返回值
     * true:加密参数，false：加密返回值
     *      example：public Result getData（Object obj）
     *          true加密obj false加密Result
     * @return
     */
    boolean encodeArg() default true;
}
