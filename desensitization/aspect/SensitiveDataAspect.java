package  .aop.desensitization.aspect;

import cn.hutool.core.collection.CollUtil;
import  .aop.desensitization.annotation.SensitiveDecode;
import  .aop.desensitization.annotation.SensitiveEncode;
import  .aop.desensitization.util.SensitiveInfoUtil;
import  .util.validate.ValidationUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * 敏感数据切面处理类
 **/
@Slf4j
@Aspect
@Component
public class SensitiveDataAspect {

    /**
     * 定义切点Pointcut
     */
    @Pointcut("@annotation( .aop.desensitization.annotation.SensitiveEncode) || @annotation( .aop.desensitization.annotation.SensitiveDecode)")
    public void sensitivePointCut() {
    }

    /**
     * 1.拿到注解后，看是加密注解还是解密注解
     * 2.加密注解的话，看是加密方法的实参，还是加密方法的返回值
     * 3.加密注解拿到的Class entity是用于指定加密的对象，且该对象需要加密的属性上需要加上@SensitiveField注解
     * 4.解密的话，只解密返回值，也需要指定解密对象
     * 5.整体流程：
     *  5.1；代码层面：在需要解加密的方法上，标注@SensitiveDecode或@SensitiveEncode注解（指定entity为对象class），在需要解加密的对象具体字段上标注@SensitiveField注解（解加密都一样）
     *  5.2：切面层面：需要注意的是，加密的对象属性可能是String Object List三种类型，故做了嵌套处理
     *
     * 6.特别说明：
     *  注解是通过spring的aop机制生效的，而aop是在类层面生效，也就是你通过一个类的方法调用另一个类的方法时，spring会检查它是否使用了切面处理，
     *  如果进入一个类的方法没有切面处理，那么在该方法中再调用该类的本地方法时，就不会再检查相关注解和切面处理
     *  也就是说，一个方法中去调另一个方法，另一个方法加了注解是不生效的
     *  example：
     *  public void a(){
     *      b(obj)
     *  }
     * @SensitiveEncode
     *  void b(T obj){
     *
     *  }
     *  此时@SensitiveEncode不生效
     * @param point
     * @return
     * @throws Throwable
     */
    @Around("sensitivePointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        // 获取方法注解信息：是哪个实体、是加密还是解密
        boolean isEncode = true;
        Class entity = null;
        Object result = null;
        boolean encodeArg = true;
        Object[] params = point.getArgs();
        //处理参数的加密（在增删改和第三方接口传入参数，存入数据库时候加密）
        MethodSignature methodSignature = (MethodSignature) point.getSignature();
        Method method = methodSignature.getMethod();
        SensitiveEncode encode = method.getAnnotation(SensitiveEncode.class);
        if(encode != null){
            encodeArg = encode.encodeArg();
        }
        //加密参数
        if(params.length >= 1 && encodeArg && encode != null){
        //待加密 Object类型
            List<Object> readyEncryptObjs = new ArrayList<>(params.length);
            // 待加密List类型
            List<Object> readyEncryptLists = new ArrayList<>(params.length);

            entity = encode.entity();

            //说明加密的是参数
            //只加密对象体，单个String和基本类型不加密
            for (Object param : params) {
                if (param.getClass().isPrimitive() || String.class.isAssignableFrom(param.getClass())) {
                    //是基本类型 直接返回 不需要处理
                    continue;
                }
                //只拿取对象类型的
                String paramStr = param.toString();
                if(paramStr.contains("(") && paramStr.contains(")")){
                    readyEncryptObjs.add(param);
                }else if(param instanceof List){
                    List list = (List)param;
                    if(list.isEmpty()){
                        continue;
                    }
                    readyEncryptLists.add(list);
                }
            }
            //只允许有一个对象，后续如果有两个对象体，这里要循环处理
            ValidationUtils.throwIf(()->readyEncryptObjs.size() > 1,"加密方法内只能有一个对象体！，如有问题请查看SensitiveDataAspect ");
            ValidationUtils.throwIf(()->readyEncryptLists.size() > 1,"加密方法内只能有一个集合体！，如有问题请查看SensitiveDataAspect ");
            //不能同时存在：当然你自己也可以处理
            ValidationUtils.throwIf(()-> CollUtil.isNotEmpty(readyEncryptLists) && CollUtil.isNotEmpty(readyEncryptObjs) ,"加密方法参数不能同时存在对象和集合！，如有问题请查看SensitiveDataAspect行左右");

            //Object（对象）类型加密
            //可能的是，1.对象只有属性 2对象包含另一个对象，3对象包含list
            if(CollUtil.isNotEmpty(readyEncryptObjs)  && CollUtil.isEmpty(readyEncryptLists)){
                Object readyEncryptObj = readyEncryptObjs.get(0);
                SensitiveInfoUtil.handlerObjectForArgs(readyEncryptObj,entity);
            }
            //list类型加密
            if(CollUtil.isNotEmpty(readyEncryptLists) && CollUtil.isEmpty(readyEncryptObjs)){
                Object readyEncryptList = readyEncryptLists.get(0);
                SensitiveInfoUtil.handleListForArgs(readyEncryptList, entity, true);
            }
        }
        // 处理结果
        result = point.proceed();
        if (result == null) {
            return result;
        }
        Class resultClass = result.getClass();
        if (resultClass.isPrimitive()) {
            //是基本类型 直接返回 不需要处理
            return result;
        }
        if(encode==null){
            SensitiveDecode decode = method.getAnnotation(SensitiveDecode.class);
            if(decode!=null){
                entity = decode.entity();
                isEncode = false;
            }
        }else{
            entity = encode.entity();
        }
        long startTime = System.currentTimeMillis();
        if (resultClass.equals(entity) ) {
            // 方法返回实体和注解的entity一样，如果注解没有申明entity属性则认为是(方法返回实体和注解的entity一样)
            SensitiveInfoUtil.handlerObject(result, isEncode);
        } else if (result instanceof List) {
            // 方法返回List<实体>
            SensitiveInfoUtil.handleList(result, entity, isEncode);
        } else {
            // 方法返回一个对象
            SensitiveInfoUtil.handlerObjectForResult(result, entity, isEncode);
        }
        long endTime = System.currentTimeMillis();
        log.info((isEncode ? "加密操作，" : "解密操作，") + "Aspect程序耗时：" + (endTime - startTime) + "ms");

        return result;
        }
    }
