# Sensitive

#### 介绍
加密解密注解，敏感字段加密，可以加密解密参数，返回值

#### 软件架构
可用于生产，自己验证
### 加密解密@SensitiveDecode，@SensitiveEncode，@SensitiveField操作说明

|   应用  | 功能   | 描述                        |
|-----|------|---------------------------|
| @SensitiveDecode  | 解密 | 添加在方法上，entity：需要解密的实体类  |
| @SensitiveEncode | 加密 | 添加在方法上，entity：需要加密的实体类（如果有嵌套，则要指明T泛型的），encodeArg：true加密方法实参，false：加密方法返回值 |
| @SensitiveField | 需要加密的属性，如果不加这个，有加解密注解，也不会进行加解密 | 添加在实体类的属性上，特别的，如果实体类嵌套了另一个实体或者list，也仅需要标注在具体的属性上 |
| AOP逻辑 | AOP：around通知 | 先拦截加解密注解，再判断是加密参数还是返回值，确定加解密的实体对象后再判断是处理那个属性 |
| 属性类型处理 | 根据不同属性类型，不同处理逻辑 | 基本类型：不处理，String：处理，Object：处理 List：处理 |
| 补充说明 | 失效问题 | 如果进入一个类的方法没有切面处理，那么在该方法中再调用该类的本地方法时，就不会再检查相关注解和切面处理，也就是说，一个方法内部再调本类方法，另一个类就算加了注解，也是不生效的：example如下：
       public void a(){
           b(obj)
       }
      @SensitiveEncode
       void b(T obj){
     
       }
       此时@SensitiveEncode不生效 |

